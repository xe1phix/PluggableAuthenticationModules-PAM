#!/bin/sh
## ================================================================================================= ##
# Have to copy the file before modifying it, as its also worked on in CIS SN.8.
# dcredit is number of numerals/digits required (1)
# lcredit is number of lower-case characters required (1)
# ocredit is number of other/special/punctuation characters required (1)
# ucredit is number of upper-case characters required (1)
## ================================================================================================= ##

cd /etc/pam.d
/bin/cp -pf /etc/pam.d/system-auth $tmpcis/system-auth.tmp
awk '( $1 == "password" && $2 == "requisite" && $3 == "pam_cracklib.so" ) \
	{ print $0 " dcredit=-1 lcredit=-1 ocredit=-1 ucredit=-1 minlen=9"; \
		next };		\
	{ print }' $tmpcis/system-auth.tmp > system-auth
chown root:root /etc/pam.d/system-auth
chmod 0644
/etc/pam.d/system-auth
echo "diff /etc/pam.d-preCIS/system-auth /etc/pam.d/system-auth"
	diff /etc/pam.d-preCIS/system-auth /etc/pam.d/system-auth
echo " system-auth {original contents} --------------------------------"
cat /etc/pam.d-preCIS/system-auth
echo " system-auth {updated contents} ---------------------------------"
cat /etc/pam.d/system-auth
echo "-----------------------------------------------------------------"

## ================================================================================================= ##
echo "Protecting a hardened copy of /etc/pam.d/system-auth"
## ================================================================================================= ##
/bin/cp -pf /etc/pam.d/system-auth /etc/pam.d/system-auth.HardenedProtectedCopy
ls -la /etc/pam.d/system-auth*
cd $cishome
chmod -R 0400 $tmpcis/*
